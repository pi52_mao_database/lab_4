﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DBLab4
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SqlConnection _sqlConnection;
        private DataSet _dataSet;

        public MainWindow()
        {
            InitializeComponent();
            _sqlConnection = new SqlConnection(ConfigurationManager
                    .ConnectionStrings["LibraryConnection"].ConnectionString);
            _dataSet = new DataSet();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            tabs.Items.RemoveAt(tabs.SelectedIndex);
        }

        private void Table_Selected(object sender, RoutedEventArgs e)
        {
            var treeView = (TreeViewItem) sender;
            treeView.Items.Clear();
            var selectScript =
                "select TABLE_NAME FROM INFORMATION_SCHEMA.TABLES where Not(TABLE_NAME like 'sysdiagrams')";
            var selectCommand = new SqlCommand(selectScript, _sqlConnection);
            _sqlConnection.Open();
            var dataReader = selectCommand.ExecuteReader();
            if (dataReader.HasRows)
            {
                while (dataReader.Read())
                {
                    var tmpTreeView = new TreeViewItem();
                    tmpTreeView.Header = dataReader[0].ToString();
                    tmpTreeView.Selected += Table_Load;
                    treeView.Items.Add(tmpTreeView);
                }
            }
            _sqlConnection.Close();
        }

        private void Table_Load(object sender, RoutedEventArgs e)
        {
            var treeView = (TreeViewItem)sender;
            var selectCommand = new SqlCommand("SELECT * from " + treeView.Header, _sqlConnection);
            _sqlConnection.Open();
            var dataAdapter = new SqlDataAdapter(selectCommand);
            dataAdapter.Fill(_dataSet, treeView.Header.ToString());
            var grid = new DataGrid();
            grid.AutoGenerateColumns = true;
            grid.ItemsSource = _dataSet.Tables[treeView.Header.ToString()].DefaultView;
            var contextMenu = new ContextMenu();
            var menuItem = new MenuItem();
            menuItem.Header = "Close";
            menuItem.Click += MenuItem_Click;
            contextMenu.Items.Add(menuItem);
            tabs.Items.Add(
                new TabItem
                {
                    Header = treeView.Header,
                    Content = grid,
                    ContextMenu = contextMenu
                });
            _sqlConnection.Close();
            Status.Text = "Завантажено таблицю " + treeView.Header;
        }

        private void Proc_Selected(object sender, RoutedEventArgs e)
        {
            var treeView = (TreeViewItem) sender;
            var sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "proc1";
            sqlCommand.Connection = _sqlConnection;
            _sqlConnection.Open();
            var dataAdapter = new SqlDataAdapter(sqlCommand);
            dataAdapter.Fill(_dataSet, "proc1");
            var grid = new DataGrid();
            grid.AutoGenerateColumns = true;
            grid.ItemsSource = _dataSet.Tables["proc1"].DefaultView;
            var contextMenu = new ContextMenu();
            var menuItem = new MenuItem();
            menuItem.Header = "Close";
            menuItem.Click += MenuItem_Click;
            contextMenu.Items.Add(
                    new TabItem
                    {
                        Header = treeView.Header.ToString(),
                        Content = grid,
                        ContextMenu = contextMenu
                    }
                );
            _sqlConnection.Close();
            Status.Text = "Завантажено таблицю " + treeView.Header;
        }

        private void Table_Expanded(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
